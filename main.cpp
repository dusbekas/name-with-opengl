#include <GLFW/glfw3.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE); // close program on ESC key
}

void setup_viewport(GLFWwindow* window)
{
    // setting viewports size, projection etc
    float ratio;
    int width, height;
    glfwGetFramebufferSize(window, &width, &height);
    ratio = width / (float) height;
    glViewport(0, 0, width, height);
    
    glClear(GL_COLOR_BUFFER_BIT);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-ratio, ratio, -1.f, 1.f, 1.f, -1.f);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void display()
{
  float reX = 0.2;
  
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_LINE_SMOOTH);
  glEnable(GL_POLYGON_SMOOTH);
  //glEnable(GL_POINT_SMOOTH);
  //Background
  glColor3ub(25,25,25);
  glBegin(GL_POLYGON);
  glVertex2f(-2, -2);
  glVertex2f(2, -2);
  glVertex2f(2, 2);
  glVertex2f(-2, 2);
  glEnd();
    
  //Line
  glColor3ub(0,200,250);
  glBegin(GL_POLYGON);
  glVertex2f(-1.7, -0.35);
  glVertex2f(1.65, -0.35);
  glVertex2f(1.65, -0.36);
  glVertex2f(-1.7, -0.36);
  glEnd();
  
  //A-l
  glLineWidth(2);
  glColor3ub(0,200,250);
  glBegin(GL_LINE_STRIP);
  glVertex2f(-1.35-reX, -0.3);
  glVertex2f(-1.45-reX, -0.3);
  glVertex2f(-1.1-reX, 0.6);
  glEnd();
  //A-m
  glBegin(GL_LINE_STRIP);
  glVertex2f(-1.2-reX, 0.1);
  glVertex2f(-1.05-reX, 0.1);
  glVertex2f(-1.075-reX, 0.3);
  glEnd();
  //A-r
  glBegin(GL_LINE_STRIP);
  glVertex2f(-1.045-reX, 0);
  glVertex2f(-1.0-reX, -0.3);
  glVertex2f(-0.9-reX, -0.3);
  glEnd();
  
  //R-l
  glBegin(GL_LINE_STRIP);
  glVertex2f(-0.7-reX, -0.3);
  glVertex2f(-0.8-reX, -0.3);
  glVertex2f(-0.6-reX, 0.6);
  //glVertex2f(-0.55, 0.6);
  glEnd();
  //R-m
  float radius = 0.175;
  const float DEG2RAD = 3.14159/180;
  glBegin(GL_LINE_STRIP);
  for (int i=0; i<200; i++){
    float degInRad = i*DEG2RAD;
    glVertex2f(sin(degInRad)*radius-0.5-reX,cos(degInRad)*radius+0.3);
  }
  glEnd();
  //R-r
  glBegin(GL_LINE_STRIP);
  glVertex2f(-0.575-reX, 0);
  glVertex2f(-0.475-reX, 0);
  glVertex2f(-0.375-reX, -0.3);
  glVertex2f(-0.275-reX, -0.3);
  glEnd();
  
  //M-l
  glBegin(GL_LINE_STRIP);
  glVertex2f(-0.05-reX, -0.3);
  glVertex2f(-0.15-reX, -0.3);
  glVertex2f(0.05-reX, 0.6);
  glEnd();
	//M-m1
  glBegin(GL_LINE_STRIP);
  glVertex2f(0.15-reX, 0.4);
  glVertex2f(0.25-reX, 0.175);
	glEnd();
  //M-m2
  glBegin(GL_LINE_STRIP);
  glVertex2f(0.575-reX, 0.6);
  glVertex2f(0.325-reX, 0.35);
	glEnd();
  //M-r
  glBegin(GL_LINE_STRIP);
  glVertex2f(0.45-reX, 0.35);
  glVertex2f(0.325-reX, -0.3);
  glVertex2f(0.425-reX, -0.3);
	glEnd();
 
  //A2-l
  glBegin(GL_LINE_STRIP);
  glVertex2f(0.625-reX, -0.3);
  glVertex2f(0.525-reX, -0.3);
  glVertex2f(0.875-reX, 0.6);
  glEnd();
  //A2-m
  glBegin(GL_LINE_STRIP);
  glVertex2f(0.805-reX, 0.1);
  glVertex2f(0.955-reX, 0.1);
  glVertex2f(0.925-reX, 0.3);
  glEnd();
  //A2-r
  glBegin(GL_LINE_STRIP);
  glVertex2f(0.96-reX, 0);
  glVertex2f(1-reX, -0.3);
  glVertex2f(1.1-reX, -0.3);
  glEnd();
  
  //N-l
  glBegin(GL_LINE_STRIP);
  glVertex2f(1.3-reX, -0.3);
  glVertex2f(1.2-reX, -0.3);
  glVertex2f(1.4-reX, 0.6);
  glEnd();
  //N-m
  glBegin(GL_LINE_STRIP);
  glVertex2f(1.45-reX, 0.25);
  glVertex2f(1.6-reX, -0.3);
  glVertex2f(1.7-reX, -0.3);
  glEnd();
  //N-r
  glBegin(GL_LINE_STRIP);
  glVertex2f(1.65-reX, 0);
  glVertex2f(1.8-reX, 0.6);
  glEnd();
  
  glDisable( GL_BLEND );
}

int main(void)
{  
    GLFWwindow* window;
    if (!glfwInit()) exit(EXIT_FAILURE);
        
    window = glfwCreateWindow(960, 480, "Hello Arman", NULL, NULL);
    
    if (!window)
    {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }
    
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);
    glfwSetKeyCallback(window, key_callback);
    
    while (!glfwWindowShouldClose(window))
    {
        setup_viewport(window);
        
        display();        
        
        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    glfwDestroyWindow(window);
    glfwTerminate();
    
    exit(EXIT_SUCCESS);
}
